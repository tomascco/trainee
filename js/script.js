$(function(){
  $(document).tooltip();
  $(".container_titulo_site").slideToggle(1200)
  $("h2").slideToggle(600)
  $("#alerta").position({
    at: "center",
    of: "header"
  })
  $("i").click(function(){
    $("#alerta").slideToggle()
  })
  contador()
  //6 segundos são pra esperar o contador
  $('#alerta').delay(6100).draggable().slideToggle();
  $("#novidadesform [type='submit']").click(function(e) {
    e.preventDefault()
    let email = $("#novidadesform [name='email']").val()
    if (email == "") {
      toastr.error('Preencha um email!', 'Error!')
    }
    else {
      $.ajax({
        url: 'http://51.254.204.44/ti/enviar_email.php',
        type: 'post',
        data: {'meuemail': email},
        dataType: 'JSON',
        success: function(resposta) {
          console.log(resposta);
          toastr.success(resposta.text)
          $("#resultado").html(email + " Foi salvo na nossa lista de novidades =)")
          $("#novidadesform [name='email']").val("")
          $("#alerta").delay(2000).slideToggle()
        },
        error: function(error) {
          console.log(error)
          toastr.error(error.responseJSON.text, 'Error!')
          $("#novidadesform [name='email']").val("")
        }
      })
    }
  })
})

function contador() {
  let i = 5
  let contador = setInterval(function(){
    $("#contador").html("Alerta em: " + i)
    if (i <= 3) {
      $("#contador").css("color", "red")
    }
    if (i == 0) {
      $("#contador").toggle()
      clearInterval(contador)
      $("html, body").animate({ scrollTop: 0 }, "slow");
      $("header").css("margin-bottom", "3em")
    }
    i--
  }, 1000)
}
